package main

import (
    "log"
    "errors"
)

func main() {
    size := float64(1024)

    // 初期状態の正方形を持った面を含む、面のslice
    faces := []Face{{
        Edges: []Edge{
            {Start: Point{X: 0, Y: 0}, End: Point{X: size, Y: 0}},
            {Start: Point{X: size, Y: 0}, End: Point{X: size, Y: size}},
            {Start: Point{X: size, Y: size}, End: Point{X: 0, Y: size}},
            {Start: Point{X: 0, Y: size}, End: Point{X: 0, Y: 0}},
        },
        Angle: 0,
        BackSide: false,
    }}

    // 折り目
    line := Line{Start: Point{X: 0, Y: 0}, End: Point{X: 1024, Y: 1024}}

    // lineで切断され、下側が線対象に反転する
    faceNum := len(faces)
    for i := 0; i < faceNum; i++ {
        face := faces[0]
        faces = faces[1:]

        for _, edge := range face.Edges {
            x, y, err := getCrossPoint(edge.Start, edge.End, line.Start, line.End)
            if err == nil {
                log.Println("交点があります", x, y)
            }
        }

        log.Println(face, faces, line)
    }
}

func getCrossPoint(edgeStart, edgeEnd, lineStart, lineEnd Point) (x, y float64, err error) {

    dev := (edgeEnd.Y - edgeStart.Y) * (lineEnd.X - lineStart.X) -
        (edgeEnd.X - edgeStart.X) * (lineEnd.Y - lineStart.Y)
    if dev != 0 {
        d1 := lineStart.Y * lineEnd.X - lineStart.X * lineEnd.Y
        d2 := edgeStart.Y * edgeEnd.X - edgeStart.X * edgeEnd.Y
        x = (d1 * (edgeEnd.X - edgeStart.X) - d2 * (lineEnd.X - lineStart.X)) / dev
        y = (d1 * (edgeEnd.Y - edgeStart.Y) - d2 * (lineEnd.Y - lineStart.Y)) / dev

        var smallX float64
        var largeX float64
        var smallY float64
        var largeY float64

        if edgeStart.X < edgeEnd.X {
            smallX = edgeStart.X
            largeX = edgeEnd.X
        } else {
            smallX = edgeEnd.X
            largeX = edgeStart.X
        }

        if edgeStart.Y < edgeEnd.Y {
            smallY = edgeStart.Y
            largeY = edgeEnd.Y
        } else {
            smallY = edgeEnd.Y
            largeY = edgeStart.Y
        }

        // edgeの線分内で、startと一致していなければOK
        if smallX <= x && x <= largeX && smallY <= y && y <= largeY &&
            !(edgeStart.X == x && edgeStart.Y == y) {
            return
        }
    }

    x = 0
    y = 0
    err = errors.New("交点がありません")
    return
}

/**
 * models
 */
type Point struct { // 点
    X float64
    Y float64
}

type Face struct { // 面
    Edges []Edge
    Angle int // 元の状態から何度傾いたか(-180 < 0 = 元 <= 180)
    BackSide bool // true: 裏, false: 表
}

type Edge struct { // 辺
    Start Point
    End Point
}

type Line struct { // 折れ線
    Start Point
    End Point
}
