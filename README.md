# _WAAW05_ origami-line

毎週1つWebアプリを作ろうの5回目。

期間は18/06/08 - 18/06/14

折り紙を開いた際にできる展開図をランダムに生成する。


## ゴール

* [ ] ランダムに紙を折る動きをシミュレーションできる
* [ ] 展開した図を画像として出力できる
* [ ] それなりに早く処理できる


## 目的

* [ ] アルゴリズムや考え方の勉強
* [ ] 画像処理
* [ ] 独自のアイコンを簡単に作るため


## 課題

* [ ] アルゴリズム
    
    * 行動: 山折り(線分)、谷折り(線分)、裏表反転(線対象)、開く(最初から1つ前のステップまでの成果物)
    * 辺と線分が交わるか
    * 面の最初の位置と折られた後の位置
    * 折られる前の位置と折られた後の位置
    * 線対象

* [ ] 画像処理


## 振り返り

0. Idea:

    アイデアやテーマについて
    * アイコン生成は個人的に永遠のテーマなので悪くないはず。


0. What went right:

    成功したこと・できたこと
    * 4点、2線の交点を導けた


0. What went wrong:

    失敗したこと・できなかったこと
    * 紙を折るという動作が実現できなかったので、そもそも何もできていない


0. What I learned:

    学べたこと
    * 折り紙ってめちゃくちゃアルゴリズムが複雑！


## サンプル

なし


## アルゴリズム

考えていたアルゴリズムを残しておく

前提: ある図形の辺の一覧と、折れ線の開始終了の座標を持っている

0. 辺を1つ取得して、折れ線と交わっているかを確認
    0. 交わっていなければ新しい図形の辺に追加
    0. 交わっていなければ辺の開始から交点までの辺を図形に追加
0. 次の辺があれば繰り返す

全然実現できないダメアルゴリズムやけどね！！
